$(document).on('scroll', function () {
  if (+window.pageYOffset >= +document.documentElement.clientHeight) {
    $('.scroll-top').css('display', 'block')
  } else {
    $('.scroll-top').css('display', 'none')
  }
})
$('.scroll-top').on('click', function () {

  $('html').animate({ scrollTop: 0 }, 800)
});

function scrollDown(id) {
  $('html').animate({
    scrollTop: $(id).offset().top
  }, 800);
}


$("#scroll-popular-posts").click(() => { scrollDown("#popular-posts") });
$("#scroll-popular-clients").click(() => { scrollDown("#popular-clients") });
$("#scroll-top-rated").click(() => { scrollDown("#top-rated") });
$("#scroll-hot-news").click(() => { scrollDown("#hot-news") });

$('.slide-toggle').on('click', function () {
  $('#hide-section').slideToggle(600)
})